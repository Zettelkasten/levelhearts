package com.zettelnet.levelhearts.zet.util;

public class TerminationException extends RuntimeException {
	private static final long serialVersionUID = 0L;

	public TerminationException() {
		super();
	}

	public TerminationException(String msg) {
		super(msg);
	}

	public TerminationException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public TerminationException(Throwable cause) {
		super(cause);
	}
}
