package com.zettelnet.levelhearts.health;

public interface HealthCallable {

	double call();
}
