package com.zettelnet.levelhearts.health.level.combat;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import com.gmail.mrphpfan.mccombatlevel.McCombatLevel;
import com.gmail.mrphpfan.mccombatlevel.PlayerCombatLevelChangeEvent;
import com.zettelnet.levelhearts.health.HealthManager;
import com.zettelnet.levelhearts.health.level.HealthLevel;

public class CombatHealthLevel implements HealthLevel, Listener {

	private final McCombatLevel mcCombatLevel;
	private final HealthManager healthManager;

	public CombatHealthLevel(Plugin plugin, McCombatLevel mcCombatLevel, HealthManager healthManager) {
		this.mcCombatLevel = mcCombatLevel;
		this.healthManager = healthManager;

		plugin.getServer().getPluginManager().registerEvents(this, plugin);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onPlayerCombatLevelChange(PlayerCombatLevelChangeEvent event) {
		if (event.getOldLevel() == -1) {
			// player had not been previously loaded
			// this case is already handled with onPlayerJoin
			return;
		}
		healthManager.getHealthTrigger().onLevelChange(event.getPlayer(), event.getOldLevel(), event.getNewLevel());
	}

	@Override
	public int get(Player player) {
		Integer level;
		while ((level = mcCombatLevel.getCombatLevel(player)) == null) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
				break;
			}
		}
		return level;
	}

	@Override
	public String getIdentifier() {
		return "combatLevel";
	}
}
