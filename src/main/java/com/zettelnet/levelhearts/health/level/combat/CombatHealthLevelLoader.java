package com.zettelnet.levelhearts.health.level.combat;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import com.gmail.mrphpfan.mccombatlevel.McCombatLevel;
import com.zettelnet.levelhearts.health.HealthManager;
import com.zettelnet.levelhearts.health.level.HealthLevelLoadException;
import com.zettelnet.levelhearts.health.level.HealthLevelLoader;

/**
 * Represents a {@link HealthLevelLoader} for {@link CombatHealthLevel}.
 * 
 * @author Zettelkasten
 * 
 */
public class CombatHealthLevelLoader implements HealthLevelLoader {

	private final Plugin plugin;

	private final PluginManager pluginManager;
	private final HealthManager healthManager;

	public CombatHealthLevelLoader(Plugin plugin, PluginManager pluginManager, HealthManager healthManager) {
		this.plugin = plugin;
		this.pluginManager = pluginManager;
		this.healthManager = healthManager;
	}

	@Override
	public CombatHealthLevel load() throws HealthLevelLoadException {
		if (!pluginManager.isPluginEnabled("McCombatLevel")) {
			throw new HealthLevelLoadException("Plugin \"McCombatLevel\" has to be enabled");
		}
		McCombatLevel mcCombatLevel = (McCombatLevel) pluginManager.getPlugin("McCombatLevel");

		return new CombatHealthLevel(plugin, mcCombatLevel, healthManager);
	}

}
